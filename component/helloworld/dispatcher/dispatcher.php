<?php
/**
 * Created by PhpStorm.
 * User: oli
 * Date: 5/13/15
 * Time: 3:54 PM
 */

namespace Nooku\Component\Helloworld;

use Nooku\Library;

class Dispatcher extends Library\Dispatcher
{
    public function canDispatch()
    {
        return true;
    }
}