<?php
/**
 * Nooku Platform - http://www.nooku.org/platform
 *
 * @copyright	Copyright (C) 2011 - 2014 Johan Janssens and Timble CVBA. (http://www.timble.net)
 * @license		GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link		https://github.com/nooku/nooku-platform for the canonical source repository
 */

namespace Nooku\Component\Application;

use Nooku\Library;
use PhpSpec\Exception\Exception;

/**
 * Module Template Filter
 *
 * Filter will parse elements of the form <html:modules position="[position]"> and render the modules that are
 * available for this position.
 *
 * Filter will parse elements of the form <html:module position="[position]">[content]</module> and inject the
 * content into the module position.
 *
 * @author  Johan Janssens <http://github.com/johanjanssens>
 * @package Nooku\Component\Pages
 */
class TemplateFilterModule extends Library\TemplateFilterAbstract
{
    /**
     * Database rowset or identifier
     *
     * @var	string|object
     */
    protected $_modules;

    /**
     * Constructor.
     *
     * @param  Library\ObjectConfig $config  An optional Library\ObjectConfig object with configuration options
     */
    public function __construct(Library\ObjectConfig $config)
    {
        parent::__construct($config);

        $this->_modules = $config->modules;
    }

    /**
     * Initializes the options for the object
     *
     * Called from {@link __construct()} as a first step of object instantiation.
     *
     * @param  Library\ObjectConfig $config  An optional Library\ObjectConfig object with configuration options
     * @return void
     */
    protected function _initialize(Library\ObjectConfig $config)
    {
        $config->append(array(
            'modules'  => 'application.modules',
            'priority' => self::PRIORITY_LOW,
        ));

        parent::_initialize($config);
    }

    /**
     * Parse <khtml:modules /> and <khtml:modules></khtml:modules> tags
     *
     * @param string $text Block of text to parse
     * @return void
     */
    public function filter(&$text)
    {
        $this->_parseModuleTags($text);
        $this->_parseInlineTags($text);
        $this->_parseModulesTags($text);
    }

    /**
     * Get the modules
     *
     * @throws	\UnexpectedValueException	If the request doesn't implement the Library\ModelEntityInterface
     * @return Library\ModelEntityInterface
     */
    public function getModules()
    {
        if(!$this->_modules instanceof Library\ModelEntityInterface)
        {
            $this->_modules = $this->getObject($this->_modules);

            if(!$this->_modules instanceof Library\ModelEntityInterface)
            {
                throw new \UnexpectedValueException(
                    'Modules: '.get_class($this->_modules).' does not implement Library\ModelEntityInterface'
                );
            }
        }

        return $this->_modules;
    }

    /**
     * Parse <ktml:module></ktml:module> tags
     *
     * @param string $text Block of text to parse
     */
    protected function _parseModuleTags(&$text)
    {
        $matches = array();
        if(preg_match_all('#<ktml:module\s+([^>]*)>(.*)</ktml:module>#siU', $text, $matches))
        {
            foreach($matches[0] as $key => $match)
            {
                //Create attributes array
                $defaults = array(
                    'params'	=> '',
                    'title'		=> '',
                    'class'		=> '',
                    'position'  => ''
                );

                $attributes = array_merge($defaults, $this->parseAttributes($matches[1][$key]));

                //Create module object
                $values = array(
                    'id'         => uniqid(),
                    'content'    => $matches[2][$key],
                    'position'   => $attributes['position'],
                    'params'     => $attributes['params'],
                    'title'      => $attributes['title'],
                    'name'       => 'dynamic',
                    'identifier' => $this->getIdentifier('com:application.module.dynamic.html'),
                    'attribs'    => (array) array_diff_key($attributes, $defaults)
                );

                $this->getModules()->create($values, Library\ModelEntityInterface::STATUS_FETCHED);
            }

            //Remove the <khtml:module></khtml:module> tags
            $text = str_replace($matches[0], '', $text);
        }
    }

    /**
     * Parse {khtml:modules [condition] ? [true string] : [false string]} tags
     *
     * Allows conditional string output, format must be condition ? true string : [optional] false string
     *
     * @see $this->_evaluateCondition() for condition format
     *
     * @param string $text Block of text to parse
     */
    protected function _parseInlineTags(&$text)
    {
        $replace = array();
        $matches = array();

        // {khtml:modules [condition] ? [string] : [string]}
        if(preg_match_all('#{khtml:modules\s+([^}]+)}#siU', $text, $matches))
        {
            $count = count($matches[1]);

            //Get all the module positions
            $positions = array();
            foreach($this->getModules() as $module){
                if(!isset($position[$module->position])){
                    $positions[$module->position] = 0;
                }
                $positions[$module->position]++;
            }

            for($i = 0; $i < $count; $i++)
            {
                $value = $matches[1][$i];
                list($condition, $options) = explode('?',$value, 2);

                //If no condition set, remove tag
                if(!trim($condition)){
                    $replace[$i] = '';
                }

                //Extract options
                $options = explode(':', $options, 2);
                $true = $options[0];
                $false = isset($options[1]) ? $options[1] : '';

                //If condition is true, use the true value, else use false value
                $replace[$i] = $this->_evaluateCondition($condition, $positions) ? $true : $false;
            }

            $text = str_replace($matches[0], $replace, $text);
        }
    }

    /**
     * Parse <ktml:modules> and <ktml:modules></ktml:modules> tags
     *
     * @param string $text Block of text to parse
     */
    protected function _parseModulesTags(&$text)
    {
        $replace = array();
        $matches = array();
        // <ktml:modules></khtml:modules>
        if(preg_match_all('#<ktml:modules\s+([^\/]*)>(.*)</ktml:modules>#siU', $text, $matches))
        {
            $count = count($matches[1]);

            //Get all the module positions
            $positions = array();
            foreach($this->getModules() as $module){
                if(!isset($position[$module->position])){
                    $positions[$module->position] = 0;
                }
                $positions[$module->position]++;
            }

            for($i = 0; $i < $count; $i++)
            {
                $attribs     = $this->parseAttributes( $matches[1][$i] );
                $position    = isset($attribs['position']) ? $attribs['position'] : null;
                $condition   = trim(isset($attribs['condition']) ? $attribs['condition'] : null);
                $replace[$i] = '';

                //If condition is set, and returns false, clear the contents
                if($condition){
                    $match = $this->_evaluateCondition($condition, $positions);
                    if(!$match){
                        $replace[$i] = '';
                        continue;
                    }

                    $replace[$i] = $matches[2][$i];
                }

                //If position supplied, render the position
                if($position){
                    $modules = $this->getModules()->find(array('position' => $position));
                    $replace[$i] = $this->_renderModules($modules, $attribs);

                    if(!empty($replace[$i])) {
                        $replace[$i] = str_replace('<ktml:modules:content>', $replace[$i], $matches[2][$i]);
                    }
                }
            }

            $text = str_replace($matches[0], $replace, $text);
        }

        $replace = array();
        $matches = array();
        // <ktml:modules position="[position]"/>
        if(preg_match_all('#<ktml:modules\s+position="([^"]+)"(.*)/>#iU', $text, $matches))
        {
            $count = count($matches[1]);

            for($i = 0; $i < $count; $i++)
            {
                $position    = $matches[1][$i];
                $attribs     = $this->parseAttributes( $matches[2][$i] );

                $modules = $this->getModules()->find(array('position' => $position));
                $replace[$i] = $this->_renderModules($modules, $attribs);
            }

            $text = str_replace($matches[0], $replace, $text);
        }
    }

    /**
     * Evaluates a string condition, checking the module counts against the condition, e.g.
     *
     * left && !right
     * or
     * left > 3 && right < 2
     *
     * @param $condition
     * @param array $positions
     * @return bool
     * @throws Library\TemplateExceptionSyntaxError
     */
    protected function _evaluateCondition($condition, array $positions)
    {
        $condition = $this->_parseCondition($condition, $positions);

        return (bool) eval($condition) ? true : false;
    }

    /**
     * Parses a string condition into a safe version, throwing exceptions for unsupported tokens
     *
     * @param $condition
     * @param array $positions
     * @return string
     * @throws Library\TemplateExceptionSyntaxError
     */
    protected function _parseCondition($condition, array &$positions)
    {
        //Compile to valid PHP
        $_tokens   = token_get_all('<?php '.$condition.'?>');

        $result = array();
        for ($i = 0; $i < sizeof($_tokens); $i++)
        {
            if(is_array($_tokens[$i]))
            {
                list($token, $content) = $_tokens[$i];

                switch ($token)
                {
                    //Ignore
                    case T_OPEN_TAG:
                    case T_CLOSE_TAG:
                    case T_WHITESPACE:
                        break;

                    case T_VARIABLE:

                        throw new Library\TemplateExceptionSyntaxError('Using a variable in module condition is not allowed');
                        break;

                    //These are allowed
                    case T_BOOLEAN_AND:
                    case T_BOOLEAN_OR:
                    case T_IS_EQUAL:
                    case T_IS_NOT_EQUAL:
                    case T_IS_IDENTICAL:
                    case T_IS_NOT_IDENTICAL:
                    case T_IS_NOT_IDENTICAL:
                    case T_IS_GREATER_OR_EQUAL:
                    case T_IS_SMALLER_OR_EQUAL:
                        $result[] = $content;
                        break;

                    //Only certain strings are allowed, assumed to be module positions
                    case T_STRING:

                        //Ensure certain keywords are not being used
                        if (substr($content, 0, 1) == '_' || 'GLOBALS' == $content || 'this' == $content || 'positions' == $content || strpos($content,'-') !== false) {
                            throw new Library\TemplateExceptionSyntaxError('Using "'.$content.'" in module condition is not allowed');
                        }

                        $result[] = '$positions[\''.$content.'\']';

                        //For missing "variables" set their count to 0
                        if(!isset($positions[$content])){
                            $positions[$content] = 0;
                        }

                        break;

                    //Numbers are allowed
                    case T_LNUMBER:
                        $result[] = $content;
                        break;

                    default:
                        throw new Library\TemplateExceptionSyntaxError('Using '.$content.' in module condition is not allowed');
                        break;
                }
            }
            else {
                $content = $_tokens[$i];
                if($content == ';'){
                    throw new Library\TemplateExceptionSyntaxError('Using '.$content.' in module condition is not allowed');
                }
                $result[] = $content;
            }
        }

        return 'return('.implode(' ', $result).');';
    }

    /**
     * Render the modules
     *
     * @param string $position  The modules position to render
     * @param array  $attribs   List of module attributes
     * @return string   The rendered modules
     */
    public function _renderModules($modules, $attribs = array())
    {
        $html  = '';
        $count = 1;
        foreach($modules as $module)
        {
            //Set the chrome styles
            if(isset($attribs['chrome'])) {
                $module->chrome  = explode(' ', $attribs['chrome']);
            }

            //Set the module attributes
            if($count == 1) {
                $attribs['rel']['first'] = 'first';
            }

            if($count == count($modules)) {
                $attribs['rel']['last'] = 'last';
            }

            $module->attribs = array_merge((array) $module->attribs, $attribs);

            //Render the module
            $content = $this->getObject($module->identifier)
                ->module($module)
                ->content($module->content)
                ->render();

            //Prepend or append the module
            if(isset($module->attribs['content']) && $module->attribs['content'] == 'prepend') {
                $html = $content.$html;
            } else {
                $html = $html.$content;
            }

            $count++;
        }

        return $html;
    }
}