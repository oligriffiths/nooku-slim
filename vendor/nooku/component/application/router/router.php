<?php
/**
 * Nooku Platform - http://www.nooku.org/platform
 *
 * @copyright	Copyright (C) 2011 - 2014 Johan Janssens and Timble CVBA. (http://www.timble.net)
 * @license		GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link		https://github.com/nooku/nooku-platform for the canonical source repository
 */

namespace Nooku\Component\Application;

use Nooku\Library;

/**
 *
 * TO BE IMPLEMENTED
 *
 *
 * Class Router
 * @package Nooku\Component\Application
 */
class Router extends Library\DispatcherRouter
{
    public function parse(Library\HttpUrlInterface $url)
	{
		// Get the path
        $path = trim($url->getPath(), '/');

        //Remove base path
        $path = substr_replace($path, '', 0, strlen($this->getObject('request')->getBaseUrl()->getPath()));

        // Set the format
        if(!empty($url->format)) {
            $url->query['format'] = $url->format;
        }

        $url->query['component'] = 'helloworld';
        $url->query['view'] = 'helloworld';

		//Set the route
		$url->path = trim($path , '/');

		return $this->_parseRoute($url);
	}

	public function build(Library\HttpUrlInterface $url)
	{
        $result = $this->_buildRoute($url);

		// Get the path data
		$route = $url->getPath();

        //Add the format to the uri
        if(isset($url->query['format']))
        {
            $format = $url->query['format'];

            if($format != 'html') {
                $url->format = $format;
            }

            unset($url->query['format']);
        }

        //Build the route
        $url->path = $this->getObject('request')->getBaseUrl()->getPath().'/'.$route;


		return $result;
	}

	protected function _parseRoute($url)
	{
		return true;
	}

	protected function _buildRoute($url)
	{
        return true;
	}
}
