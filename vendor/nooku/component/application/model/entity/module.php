<?php
/**
 * Nooku Platform - http://www.nooku.org/platform
 *
 * @copyright	Copyright (C) 2011 - 2014 Johan Janssens and Timble CVBA. (http://www.timble.net)
 * @license		GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link		http://github.com/nooku/nooku-platform for the canonical source repository
 */

namespace Nooku\Component\Application;

use Nooku\Library;

/**
 * Module Model Entity
 *
 * @author  Stian Didriksen <http://github.com/stipsan>
 * @package Nooku\Component\Application
 */
class ModelEntityModule extends Library\ModelEntityAbstract
{
	/**
     * Return an associative array of the data.
     *
     * @return array
     */
    public function toArray()
    {
        $data = parent::toArray();

        $data['title']       = (string) $this->title;
        $date['description'] = (string) $this->description;
        return $data;
    }
}