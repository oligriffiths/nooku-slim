<?
/**
 * Nooku Platform - http://www.nooku.org/platform
 *
 * @copyright	Copyright (C) 2011 - 2014 Johan Janssens and Timble CVBA. (http://www.timble.net)
 * @license		GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link		http://github.com/nooku/nooku-platform for the canonical source repository
 */
?>

<!DOCTYPE HTML>
<html lang="<?= $language; ?>">
<?= import('page_head.html') ?>

<body>
<header class="container">
    <nav class="navbar navbar-default">
        <a class="navbar-brand" href="/"><?= escape(object('application')->getTitle()) ?></a>
        <ktml:modules position="menu"/>
    </nav>
</header>

<div class="container">
    <div class="row">

        <ktml:modules position="left" chrome="wrapped">
            <aside class="sidebar col-md-3">
            <ktml:modules:content>
            </aside>
        </ktml:modules>

        <div class="{khtml:modules left ? col-md-9 : col-md-12}">

        <ktml:modules position="breadcrumb">
            <ktml:messages>
            <section>
                <ktml:content>
            </section>
        </div>
    </div>
</div>

</body>
</html>